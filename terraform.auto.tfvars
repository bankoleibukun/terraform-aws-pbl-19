region = "us-east-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

enable_classiclink = "false"

enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = "ami-0714556c209d25c7f"

ami-bastion = "ami-0476f5253e5f82b62"

ami-nginx = "ami-0ce09d64edb8cf78c"

ami-sonar = "ami-0dcf66e95977acd75"

keypair = "beekay"

master-password = "bankypblproject"

master-username = "banky"

account_no = "381492104614"

tags = {
  Owner-Email     = "ibukunolu.banky@gmail.com"
  Managed-By      = "ibkterraform"
  Billing-Account = "381492104614"
}